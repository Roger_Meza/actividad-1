 $(function () {
     $("[data-toggle='tooltip']").tooltip();
     $("[data-toggle='popover']").popover();
     $(".carousel").carousel({
         interval: 1500
     });

     $('#contactoBtn').on('show.bs.modal', function (e) {
         console.log('el modal contacto se esta mostrando');
         $('#contactoBtn').removeClass('btn-outline-success');
         $('#contactoBtn').addClass('btn-primary');
         $('#contactoBtn').prop('disabled', true);
     });

     $('#contactoBtn').on('shown.bs.modal', function (e) {
         console.log('el modal contacto se mostrando');
     });

     $('#contactoBtn').on('hide.bs.modal', function (e) {
         console.log('el modal contacto se oculta');
     });

     $('#contactoBtn').on('hidden.bs.modal', function (e) {
         console.log('el modal contacto se oculto');
         $('#contactoBtn').prop('disabled', false);
     });
 });